import socket
import time
from settings import HOST, PORT, ENCODING


class Client():
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.buffer_size = 4096
        self.is_userlist_checkd = False
        self.cmd = ''
        self.work = True

        self.login = ''
        self.login_list = []
        self.run()

    def run(self):
        self.socket.connect((HOST, PORT))
        self.socket.settimeout(1)
        print('You are in.')
        self.set_login()
        self.help()
        while self.work:
            try:
                try:
                    data = self.socket.recv(self.buffer_size)
                except socket.error:
                    data = ''
                self.process_data(data)
                if not self.is_userlist_checkd:
                    self.cmd = input('Enter command: ')
                    self.is_userlist_checkd = True
                    continue

                if self.cmd == 'help':
                    self.help()

                elif self.cmd == 'check':
                    pass

                elif self.cmd == 'show users':
                    self.show_users()

                elif self.cmd == 'send':
                    self.send()

                else:
                    print('Wrong command.')

                self.is_userlist_checkd = False

            except KeyboardInterrupt:
                data = 'logout'.encode(ENCODING)
                self.socket.send(data)
                self.socket.close()
                self.work = False

    def set_login(self):
        username = input('Enter your username: ')
        self.login = username
        self.socket.send(b'login;' + username.encode(ENCODING))

    def show_users(self):
        print('Active users:')
        print(', '.join(self.login_list))

    def send(self):
        msg = input('Enter your message: ')
        self.show_users()
        target = input('Enter username: ')
        while target not in self.login_list:
            target = input('Pick an active user: ')
        data = f'msg;{self.login};{target};{msg}'.encode(ENCODING)
        self.socket.send(data)

    def process_data(self, data):
        if data:
            messages = data.decode(ENCODING).split('\n')

            for msg in messages:
                if msg != '':
                    msg = msg.split(';', 3)

                    if msg[0] == 'msg':
                        print(f'from {msg[1]} to {msg[2]}')
                        print(msg[3])

                    elif msg[0] == 'login_list':
                        logins = ';'.join(msg[1:])
                        self.login_list = logins.split(';')

                    elif msg[0] == 'login':
                        print(f'from {msg[1]} to {msg[2]}')
                        print(msg[3])
                        msg[3].split(' to ')
                        self.login = msg[3][-1]

                    elif msg[0] == 'down':
                        self.socket.close()
                        input('Server is shutdown. Press ENTER to exit')
                        self.work = False

    def help(self):
        print("""
        You can use this commands:
        send - send a message to user/all users
        check - checking new messages
        show users - show list of active users
        help - show this message again
        Press Ctrl+C to exit
        """)


if __name__ == '__main__':
    client = Client(HOST, PORT)

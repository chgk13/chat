import queue
import select
import socket
from settings import HOST, PORT, ENCODING


class Server():
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.buffer_size = 2048
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.login_dict = {}

        self.inputs = [self.socket]
        self.outputs = []
        self.message_queues = {}

        self.exit = False
        try:
            self.socket.bind((self.host, self.port))
            self.socket.listen(10)
            # self.socket.setblocking(False)
        except socket.error:
            self.exit = True

        self.run()

    def run(self):
        if self.exit:
            raise Exception("Can't bind to socket")
        print('Server is running. Press Ctrl+C to shutdown.')
        while self.inputs:
            try:
                read, write, err = select.select(self.inputs, self.outputs, [])

                for r in read:
                    if r is self.socket:
                        connection, address = r.accept()

                        self.inputs.append(connection)
                        self.outputs.append(connection)
                        self.message_queues[connection] = queue.Queue()
                    else:
                        data = r.recv(self.buffer_size)
                        self.process_data(data, r)

                for w in write:
                    if w in self.outputs:
                        if not self.message_queues[w].empty():
                            data = self.message_queues[w].get()
                            w.sendall(data)

                for e in err:
                    self.inputs.remove(e)
                    if e in self.outputs:
                        self.outputs.remove(e)
                    del self.message_queues[e]
                    e.close()

                    for login, address in self.login_dict.items():
                        if address == e:
                            del self.login_dict[login]
                            break

                    self.update_login_list()

            except KeyboardInterrupt:
                print('Server is shutting down')
                for conn in self.outputs:
                    conn.send(
                        b'down;')
                    conn.close()
                self.socket.close()
                break

    def process_data(self, data, sock):
        if data:
            message = data.decode(ENCODING)
            message = message.split(';', 3)

            if message[0] == 'login':
                tmp_login = message[1]
                while message[1] in self.login_dict:
                    message[1] += '1'
                if tmp_login != message[1]:
                    login_msg = f'login;server;{message[1]};Login {tmp_login} \
is already in use. Your login is changed to {message[1]}.'
                    self.message_queues[sock].put(login_msg.encode(ENCODING))
                self.login_dict[message[1]] = sock
                print(f'{message[1]} joined the chat.')

                self.update_login_list()

            elif message[0] == 'logout':
                log = sock
                for login, address in self.login_dict.items():
                    if sock == address:
                        log = login
                        break
                print(f'{log} is out.')
                self.inputs.remove(sock)
                if sock in self.outputs:
                    self.outputs.remove(sock)
                del self.message_queues[sock]
                del self.login_dict[log]
                sock.close()
                self.update_login_list()

            elif message[0] == 'msg' and message[2] != 'all':
                msg = data + b'\n'
                target = self.login_dict[message[2]]
                self.message_queues[target].put(msg)

            elif message[0] == 'msg':
                msg = data + b'\n'
                for conn, conn_queue in self.message_queues.items():
                    if conn != sock:
                        conn_queue.put(msg)

            else:
                self.inputs.remove(sock)
                if sock in self.outputs:
                    self.outputs.remove(sock)
                del self.message_queues[sock]
                sock.close()

                for login, addr in self.login_dict.items():
                    if addr == sock:
                        del self.login_dict[login]
                        break

            self.update_login_list()

    def update_login_list(self):
        logins = 'login_list;' + ';'.join(list(self.login_dict)) + ';all\n'
        logins = logins.encode(ENCODING)
        for conn_queue in self.message_queues.values():
            conn_queue.put(logins)


if __name__ == '__main__':
    server = Server(HOST, PORT)
